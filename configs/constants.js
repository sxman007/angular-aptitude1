var appRoot = require('app-root-path');
module.exports={
    'getDataPath':function(){
        var path = null;
        if(process.platform==="win32"){
            path = appRoot + "\\storage\\data.json";
        } else {
        	path=appRoot+"/storage/data.json";
        }
        return path;
    },
};

