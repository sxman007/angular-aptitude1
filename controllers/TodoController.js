var fs = require("fs");
module.exports=function TodoController(){
    this.add=function(req,res,next){
        var content=this.readData();
        var temp={
            'id':new Date().getTime(),
            'name':req.body.name,
            'deadline':req.body.deadline,
            'importance':req.body.importance,
            'is_complete':0
        };
        content.push(temp);
        var path=CONSTANTS.getDataPath();
        fs.writeFile(path, JSON.stringify(content), 'utf8', function(){
            return res.json({message:'Updated','status':'success'});
        });
    };
    this.mark_complete=function(req,res,next){
        var content=this.readData();
        var index = -99;
        for(var i=0;i<content.length;i++)
        {
            if(req.body.id==content[i].id){
                index=i;
                break;
            }
        }
        if(index<0)
        {
            return res.json({message:'Todo not found','status':'error'});
        }
        var temp=content[index];
        temp.is_complete=1;
        content[index]=temp;
        var path=CONSTANTS.getDataPath();
        fs.writeFile(path, JSON.stringify(content), 'utf8', function(){
            return res.json({message:'Updated','status':'success'});
        });
    };
    this.readData=function(){
        var content=null;
        var path=CONSTANTS.getDataPath();
        try{
            content=require(path);
            return content;
        }
        catch(e)
        {
            return res.json({message:e,'status':'error'});
            return null;
        }
    };
};

