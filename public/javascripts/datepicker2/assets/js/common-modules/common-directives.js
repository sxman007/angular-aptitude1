app.directive("numericInput", function(){
    return {
        restrict: "A",
        link: function(scope, element, attrs){
            element.on("keydown", function(e){
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                     // Allow: Ctrl+A, Command+A
                    (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                     // Allow: home, end, left, right, down, up
                    (e.keyCode >= 35 && e.keyCode <= 40)) {
                         // let it happen, don't do anything
                         return;
                }
                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });

        }
    }
});

app.directive("validFile", function(){
    return {
        restrict: "A",
        link: function(scope, element, attrs){
            element.on("change", function(){
                console.log("Run");
            });
        }
    }
});

app.directive("myDirective", function() {
  return {

    scope: {
        user: '=data'
    },
    restrict: 'E',
    controller: function($scope) {
      //function used on the ng-include to resolve the template
      $scope.getTemplateUrl = function() {

          return "views/mobile/contactlistpart.html";
      }
    },
    templateUrl: "getTemplateUrl()"
  };
});
app.directive("chatMessageInputBox", function(){
    return {
        restrict: "A",
        link: function(scope, element, attrs){
            element.bind('keyup', function(event){

                //var keycode = (event.keyCode ? event.keyCode : event.which);
                var clickedChatRoomObj = JSON.parse(window.localStorage.getItem("clickedChatRoomObj"));
                scope.onChatInputKeyup(clickedChatRoomObj.room_id);
                //console.log(clickedChatRoomObj.room_id);

                /*scope.userChatMessageTypingDelay(function(){
                  scope.onChatInputKeyup(clickedChatRoomObj.room_id);
              }, 1200 );*/
                /*setTimeout(function(){
                    scope.$apply(function(){
                        scope.userIsTyping.typing = true;
                    });

                }, 0);*/
                /*scope.userIsTyping.typing = true;
                console.log(scope.userIsTyping);

                scope.onChatInputKeyup(clickedChatRoomObj.room_id);
                clearTimeout(scope.userChatMessageTypingDelay);
                scope.userChatMessageTypingDelay = setTimeout(function(){
                    //scope.userIsTyping.typing = false;
                }, 2000);*/

            });
        }
    }
});
app.directive("onFinishRender", function(){
    return {
        restrict: 'C',
        link: function(scope, element, attrs){
            if (scope.$last === true) {
                setTimeout(function(){
                    $(element).parent().animate({
                          scrollTop: $(element).parent().get(0).scrollHeight
                    }, 0, function(){

                    });
                }, 0);
            }
        }
    }
});
app.directive("chatHistory", function(){
    return {
        restrict: "C",
        link: function(scope, element, attrs){
            element.bind("scroll", function(){
                scrollDivHeight = $(element).scrollTop();
                if(scrollDivHeight == 0){
                    //console.log("Reach to Top");
                    scope.loadPreviousChatHistory(function(messageCount){
                        setTimeout(function(){
                            //console.log("Run..."+messageCount);
                            //console.log($(element).get(0).scrollHeight);
                            var chatBodyPositionFromTopOfTheScreen = $(".chat-body").offset().top;
                            var positionOfTargetElement = $(element).find('.conversation:nth-child('+messageCount+')').offset().top;
                            //console.log(scrollHeightOfTargetElement);
                            var scrollHeightOfTargetElement = positionOfTargetElement - chatBodyPositionFromTopOfTheScreen;
                            $(element).animate({
                                  scrollTop: scrollHeightOfTargetElement
                            }, 0);
                        },200);
                    });

                }
                //console.log(scrollDivHeight);
            });
        }
    }
});
