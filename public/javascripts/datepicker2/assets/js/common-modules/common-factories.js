(function(angular, undefined){
    'use strict';

    var cf = angular.module('commonFactory', ['ngDialog','baseConfig']);

    cf.factory('commonFactory', function () {

	var serviceObj = {};
	serviceObj.function1 = function () {
	    console.log('function1');
	};

	serviceObj.function2 = function () {
	    console.log('function2');
	};

	return serviceObj;
    });
    /**
    * Factory:  getLangFile
    * @desc :will read language json file
    * @return  {object}
    * @use i,e  inject first in controller and call it(getLangFile.langFileInfo)
    * by sunil kumar sahu
    */
    cf.factory('getLangFile',function($http){
	var serviceObj = {};
	serviceObj.langFileInfo = function(fileName){
	    return $http.get('includes/'+fileName);

	}
	return serviceObj;
    });
     /**
    * Factory:  dialogBox
    * @desc :have all the dialog box related things
    * by sunil kumar sahu
    */
    cf.factory('dialogBox',function($rootScope,ngDialog,BaseConfig){
	var serviceObj = {};
	/*
	@function:  getLangFile
	* @argument  {string}:message to show in dialog box
	* @use i,e  inject first in controller and call it(getLangFile.showCommonDialogBox)
	* by sunil kumar sahu
	*/
	serviceObj.showCommonDialogBox = function(msg){
		$rootScope.errMsg = msg;
                ngDialog.open({
                    scope: $rootScope,
                    template: BaseConfig.cmnErrorTmpl
                });

	};
	serviceObj.showDialogWithId = function(tmplId){
                ngDialog.open({
                    scope: $rootScope,
                    template: tmplId
                });

	};
	serviceObj.close = function(){

	};
	return serviceObj;
    });
    /*
	@factory:  AjaxService
	* @desc : will comunicate with server
	* @use i,e  inject first in controller and call it(AjaxService.ajax)
	* @param {object} userData the json object with data
	* @param {string} apiUrl The url
	* by sunil kumar sahu
    */
    cf.factory('AjaxService', function ($http, $q) {
        return {
            ajax: function (apiUrl, userData) {

                //Creating a deferred object
                var deferred = $q.defer();

                //Calling api
                $http.jsonp(apiUrl, {
                    params: userData,
                    timeout: 5000,
                    warningAfter: 2000
                }).success(function (data) {
                    //Passing data to deferred's resolve function on successful completion
                    deferred.resolve(data);
                })
                        /*.error(function() {
                         //Sending a friendly error message in case of failure
                         deferred.reject("An error occured while fetching items");
                         })*/
                        ;
                //Returning the promise object
                return deferred.promise;
            }
        }
    });

    cf.factory('checkValidation', function ($rootScope,ngDialog,BaseConfig) {

	var serviceObj = {};
	serviceObj.validateField = function(validationRules){
	    for (var i = 0; i < validationRules.length; i++) { // all validation rules and message
                    var validateObj = validationRules[i];
                    for (var key in validateObj) {
                        if (validateObj.hasOwnProperty(key)) {

                            var validationType = key;
                            var valueAndMsg = validateObj[key];
                            if (validationType == 'required') {
                                var returnedObj = this.required(valueAndMsg);
                            } else if (validationType == 'email') {
                                var returnedObj = this.email(valueAndMsg);
                            } else if (validationType == 'alpha') {
                                var returnedObj = this.alpha(valueAndMsg);
                            } else if (validationType == 'alpha_numeric') {
                                var returnedObj = this.alphaNumeric(valueAndMsg);
                            } else if (validationType == 'integer') {
                                var returnedObj = this.integer(valueAndMsg);
                            } else if (validationType == 'matches') {
                                var returnedObj = this.matches(valueAndMsg);
                            } else if (validationType == 'min_length') {
                                var returnedObj = this.minLength(valueAndMsg);
                            } else if (validationType == 'max_length') {
                                var returnedObj = this.maxLength(valueAndMsg);
                            } else if (validationType == 'exact_length') {
                                var returnedObj = this.exactLength(valueAndMsg);
                            }

                            var status = returnedObj.validate; // validation status
                            var errMsg = returnedObj.errMsg; // validation error message to user

                            if (returnedObj.validate == false) {

                                $rootScope.errMsg = errMsg;
                                ngDialog.open({
                                    scope: $rootScope,
                                    template:  BaseConfig.cmnErrorTmpl
                                });
                                return false;
                                break;

                            }

                        }

                    }
                }
                return status;
	};
	serviceObj.required = function(valueAndMsg){ //Returns FALSE if the form element is empty.
	        var returnType = '';
                var inputValue = valueAndMsg[0];
                var msg = valueAndMsg[1];
                if (!inputValue) {
                    returnType = false;
                } else {
                    returnType = true;
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
	};
	serviceObj.matches = function(valueAndMsg){ // Returns FALSE if the form element does not match the one in the parameter.
	        var returnType = '';
                var firstValue = valueAndMsg[0];
                var secondValue = valueAndMsg[1];
                var msg = valueAndMsg[2];

                if (!firstValue) {
                    returnType = false;
                } else if (!secondValue) {
                    returnType = false;
                } else if (firstValue != secondValue) {
                    returnType = false;
                } else {
                    returnType = true;
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
	};
	serviceObj.email = function(valueAndMsg){ // Returns FALSE if the form element does not contain a valid email address.
	        var returnType = '';
                var inputValue = valueAndMsg[0];
                var msg = valueAndMsg[1];

                if (!inputValue) {
                    returnType = false;
                } else {
                    var mailFormat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                    if (inputValue.match(mailFormat)) {
                        returnType = true;
                    } else {
                        returnType = false;
                    }
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
	};
	serviceObj.minLength = function (valueAndMsg) { //Returns FALSE if the form element is shorter then the parameter value.

                var returnType = '';
                var inputValue = valueAndMsg[0];
                var minimumLenChk = valueAndMsg[1];
                var msg = valueAndMsg[2];

                if (!inputValue) {
                    returnType = false;
                } else if (inputValue.length < minimumLenChk) {

                    returnType = false;
                } else {
                    returnType = true;
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
        };
	serviceObj.maxLength= function (valueAndMsg) { //Returns FALSE if the form element is longer then the parameter value.

                var returnType = '';
                var inputValue = valueAndMsg[0];
                var maxLenChk = valueAndMsg[1];
                var msg = valueAndMsg[2];


                if (!inputValue) {
                    returnType = false;
                } else if (inputValue.length > maxLenChk) {

                    returnType = false;
                } else {
                    returnType = true;
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
        };
	serviceObj.exactLength = function (valueAndMsg) { //Returns FALSE if the form element is longer then the parameter value.


                var returnType = '';
                var inputValue = valueAndMsg[0];
                var exactLenChk = valueAndMsg[1];
                var msg = valueAndMsg[2];


                if (!inputValue) {
                    returnType = false;
                } else if (inputValue.length != exactLenChk) {

                    returnType = false;
                } else {
                    returnType = true;
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
        };
	serviceObj.alpha= function (valueAndMsg) { //Returns FALSE if the form element contains anything other than alphabetical characters.

                var returnType = '';
                var inputValue = valueAndMsg[0];
                var msg = valueAndMsg[1];

                var alphaType = /^[a-zA-Z\s]+$/;
                if (!inputValue) {
                    returnType = false;
                } else if (!inputValue.match(alphaType)) {
                    returnType = false;
                } else {
                    returnType = true;
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
        };
	serviceObj.alphaNumeric = function (valueAndMsg) { //Returns FALSE if the form element contains anything other than alpha-numeric characters.


                var returnType = '';
                var inputValue = valueAndMsg[0];
                var msg = valueAndMsg[1];


                var alphaNumericType = /^[a-zA-Z0-9]+$/;
                if (!inputValue) {
                    returnType = false;
                } else if (!inputValue.match(alphaNumericType)) {
                    returnType = false;
                } else {
                    returnType = true;
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
        };
	serviceObj.integer = function (valueAndMsg) { //Returns FALSE if the form element contains anything other than integer.

                var returnType = '';
                var inputValue = valueAndMsg[0];
                var msg = valueAndMsg[1];
                // var integerType = /^\+?(0|[1-9]\d*)$/;
                // regex changed for added support of 0 at beginning and removed support of negetive integers
                var integerType = /^\+?(0|[0-9]\d*)$/;
                if (!inputValue) {
                    returnType = false;
                } else if (!integerType.test(inputValue)) {
                    returnType = false;
                } else {
                    returnType = true;
                }
                var returnedObj = {
                    validate: returnType,
                    errMsg: msg
                };
                return returnedObj;
	}
	return serviceObj;
    });
    /* factory for validate fields*/

    /**
    * Factory:  generateOtp
    * @desc :will generate 6 digit otp
    * @return  {numeric}
    * @use i,e  inject first in controller and call it(generateOtp.langFileInfo)
    * by sunil kumar sahu
    */
    cf.factory('otp',function($http){
	var serviceObj = {};
	serviceObj.generateOtp = function(){
	    var length = 6;
	    var generatedOtp = Math.floor(Math.pow(10, length-1) + Math.random() * (Math.pow(10, length) - Math.pow(10, length-1) - 1));
	    return generatedOtp;

	}
	return serviceObj;
    });


}(angular));
