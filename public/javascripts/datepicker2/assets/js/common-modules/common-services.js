


   // *********************************************** configService for base url ********************
   app.service('configService', function () {

        this.siteUrl = "http://192.168.0.108:3000/api_v1/";
        this.socketConnectUrl = "http://192.168.0.108:3001/";
        //this.siteUrl = "api_v1/"
        this.supportedProfilePicFormats = new Array('gif','png','jpg','jpeg');
        this.uploadFileLimit = ((1 * 1024) * 1024) * 5; //5MB
   });

   app.service('clientSideGenerator', function(){
        this.length = 8;
        this.timestamp = +new Date;

        var _getRandomInt = function( min, max ) {
    		return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
        }

        this.generate = function() {
			 var ts = this.timestamp.toString();
			 var parts = ts.split( "" ).reverse();
			 var id = "";

			 for( var i = 0; i < this.length; ++i ) {
				var index = _getRandomInt( 0, parts.length - 1 );
				id += parts[index];
			 }

			 return id;
	    }
   });

   app.service("logInCheck", function(){
       this.isLoggedIn = function(){
           var yuvitimeStorageData = JSON.parse(localStorage.getItem("yuvitimeStorage"));
           if(yuvitimeStorageData == null){
               return false;
           }
           else{
               return true;
           }
       }
   });

   app.service("stringFunctions", function(){
      this.getLength = function(inputValue){
        if(inputValue != undefined){
          return inputValue.toString().length;
        }
        else{
          return 0;
        }
      }
   });

   // *********************************************** postService for http call **********************
   app.service('postService', function($q,$http){
      var methodType="POST";
      this.call = function(postData,postUrl,headers) {
        var deferred = $q.defer();
        $http({
           method: methodType,
           url: postUrl,
           headers: headers,
           transformRequest: function(obj) {
               var str = [];
               for(var p in obj)
               str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
               return str.join("&");
           },
           data: postData
        }).success(function(data) {
            deferred.resolve(data);
        }).error(function(data) {
            deferred.reject(data);
        });
        return deferred.promise;
      };
   });

   // *********************************************** flashFlags *************************************
   app.service("flashFlags", function(){
       this.processComplete = false;
       this.message = "";
   });

   // *********************************************** api service for all api ************************
   app.service("api", function(){
       this.history="call_history/list";
       this.allContacts="all/contact_list";
       this.contactList = "contact_book/list";
       this.login="user/login";
       this.signup="user/signup";
       this.inviteFriend = "invitation/send";
       this.addContact = "contact_book/add";
       this.callDetails = "call_history/call_details";
       this.callHistoryDelete="call_history/call_details_delete_one";
       this.callHistoryDeleteAll="call_history/call_details_delete_all";
       this.invitedList="invitation/list";
       this.invitedFriendDelete="invitation/delete";
       this.facebookSignup = "user/facebook_signup";
       this.googleSignup = "user/google_signup";
       this.forgetPassword = "user/request_forget_password";
       this.varifyForgetPassword="user/verify_forget_password_token";
       this.emailVerify="user/verification";
       this.resendEmailVerify="user/resend_verification";
       this.groupDetails="group/details";
       this.newPassword="user/set_new_password";
       this.profileImageUpload="profile/upload_image";
       this.profileEdit="profile/edit_profile";
       this.profileGet="profile/get_profile";
       this.changeGroupName="group/change_name";
       this.facebookLogin = "user/facebook_login";
       this.googleLogin = "user/google_login";
       this.linkedinSignup = "user/linkedin_signup";
       this.favouriteList = "favourite/list";
       this.leaveGroup = "group/leave_group";
       this.removeUser="group/remove_user";
       this.linkedinLogin = "user/linkedin_login";
       this.addUser="group/add_members";
       this.changeRole="group/change_role";
       this.favourite="invitation/favourite";
       this.groupCallInvitation="group/call_invitation";
       this.createGroup = "group/create";
       this.contactDelete="contact_book/delete";
       this.contactDetails="contact_book/details";
       this.logout="user/logout";
       this.groupImage = "group/change_image";
       this.chatList="chat/list";
       this.getChatMessages = "chat/get_messages";
   });
   app.service("message", function(){
       this.emptyHistory = "You don't have any call history !";
       this.roleChoseError = "No role selected !";
       this.GroupSelectError ="No Group selected ";
       this.favouriteAdded="Favourite added successfully";
       this.favouriteRemove="Favourite removed successfully";
       this.emptyFavourite="No Favourite";
       this.logoutMessage="You have successfully logged out!";
   });
   app.service("facebookAppCredentials", function(){
       this.appId = '1634815040145709'
   });
   app.service("googleAppCredentials", function(){
       this.clientId = '1001654417271-er91apeu2omjprkhbr4c64n50gr94bf3.apps.googleusercontent.com'
   });

   //*********************************************** File Cropping related functions *****************
   app.service("fileCrop", function(){
       this.uploadedFileName = "";
       this.readURL = function(input, selector){
           if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#'+selector).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
       }
       this.onPreloadComplete = function(imgObject, newWidth, newHeight, startX, startY, ratio, crop_container_height, crop_container_width, image_field_name){
            var newImg = this.getImagePortion(imgObject, newWidth, newHeight, startX, startY, ratio, crop_container_height, crop_container_width);
            var blob = this.dataURItoBlob(newImg);
            var fd = new FormData();
            fd.append(image_field_name, blob, this.uploadedFileName);
            return fd;
       }
       this.getBase64Image = function(img){
           return img.replace(/^data:image\/(png|jpg);base64,/, "");
       }
       this.getImagePortion = function(imgObj, newWidth, newHeight, startX, startY, ratio, crop_container_height, crop_container_width){
            var tnCanvas = document.createElement('canvas');
            var tnCanvasContext = tnCanvas.getContext('2d');
            tnCanvas.width = newWidth;
            tnCanvas.height = newHeight;
            var bufferCanvas = document.createElement('canvas');
            var bufferContext = bufferCanvas.getContext('2d');
            bufferCanvas.width = crop_container_width;
            bufferCanvas.height = crop_container_height;
            bufferContext.drawImage(imgObj, 0, 0, crop_container_width, crop_container_height);
            tnCanvasContext.drawImage(bufferCanvas, startX, startY, newWidth, newHeight, 0, 0, newWidth, newHeight);
            return tnCanvas.toDataURL();
       }
       this.dataURItoBlob = function(dataURI){
            var byteString;
            if (dataURI.split(',')[0].indexOf('base64') >= 0)
            byteString = atob(dataURI.split(',')[1]);
            else
            byteString = unescape(dataURI.split(',')[1]);
            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
            var ia = new Uint8Array(byteString.length);
            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }
            return new Blob([ia], {type:mimeString});
       }
       this.getBase64 = function(file){

           var reader = new FileReader();
           reader.readAsDataURL(file);
           reader.onload = function () {
             //console.log(reader.result);
           };
           reader.onerror = function (error) {
             //console.log('Error: ', error);
           };
       }
   });
   app.service("commonFunctions", function(){
       this.getParameterFromUrl = function(name, url){
           if (!url) {
             url = window.location.href;
           }
           name = name.replace(/[\[\]]/g, "\\$&");
           var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
               results = regex.exec(url);
           if (!results) return null;
           if (!results[2]) return '';
           return decodeURIComponent(results[2].replace(/\+/g, " "));
       };
       this.isValidEmailAddress = function(emailAddress){
           var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
           return pattern.test(emailAddress);
       },
       this.formatAMPM = function(timestamp){
           var date = new Date(timestamp);
           var hours = date.getHours();
           var minutes = date.getMinutes();
           var ampm = hours >= 12 ? 'pm' : 'am';
           hours = hours % 12;
           hours = hours ? hours : 12; // the hour '0' should be '12'
           minutes = minutes < 10 ? '0'+minutes : minutes;
           var strTime = hours + ':' + minutes + ' ' + ampm;
           return strTime;
       }
   });

  app.service("socket", function(configService){
     this.eventList = [];
     //this.eventList.push("Hi");
     //console.log(this.eventList);
     this.socket = null;
     this.init = function(){
         if(this.socket == null){
             var yuvitimeStorage = JSON.parse(localStorage.getItem('yuvitimeStorage'));
             if(yuvitimeStorage != null){
                 this.socket = io.connect(configService.socketConnectUrl , {
                     query: {Authorization: yuvitimeStorage.auth_token}
                 });
             }
         }
     }
     this.on = function(eventName, callback){
         console.log("**********in***********");
         console.log($.inArray(eventName, this.eventList));
         if($.inArray(eventName, this.eventList) == -1){
             console.log("NO");
             this.init();
             var self = this;
             console.log(self.eventList);
             self.eventList.push(eventName);
             this.socket.on(eventName, function(data){
                 console.log("Listner....");
                 //console.log(data);
                 callback(data);
             });
         }
         else{
             console.log("HAS");
         }

     }
     this.emit = function(eventName, data, callback){

         /*if($.inArray(eventName, this.eventList) == -1){
             console.log("first");
             this.eventList.push(eventName);

             this.socket.emit(eventName, data);
         }
         else{
             console.log("two");
         }

         console.log("Emit...");*/
         this.socket.emit(eventName, data);
     }
  });
