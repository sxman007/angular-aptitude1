 (function(window, angular, undefined) {
  
  'use strict';
  var ngDevice = angular.module('ngDevice', []);
  
  ngDevice.provider('ngDevice', function() {
                    
                    this.$get = ['$document', '$templateCache', '$compile', '$q', '$http', '$rootScope', '$timeout', '$window', '$controller',/*'cameraphoto',*/
                                 function($document, $templateCache, $compile, $q, $http, $rootScope, $timeout, $window, $controller/*,cameraphoto*/) {
                                 
                                 
                                 
                                 var publicMethods = {
                                 
                                 open: function() {
                                 console.log('open');
                                 //aaa();
                                 
                                 },
                                 getCordova: function() {
                                 return device.cordova;
                                 },
                                 getModel: function() {
                                 return device.model;
                                 },
                                 getPlatform: function() {
                                 return device.platform;
                                 },
                                 getUuid: function() {
                                 return device.uuid;
                                 },
                                 getVersion: function() {
                                 return device.version;
                                 },
                                 getAllDeviceDtls: function() {
                                 var cordova = device.cordova;
                                 var model = device.model;
                                 var platform = device.platform;
                                 var uuid = device.uuid;
                                 var version = device.version;
                                 var allDeviceDtls = {
                                 cordova: cordova,
                                 model: model,
                                 platform: platform,
                                 uuid: uuid,
                                 version: version
                                 };
                                 return allDeviceDtls;
                                 },
                                 
                //For Reg/Chat Camera Function start
                                 // For Reg bigg image camera open done
                                 cameopenmainimgeformobile: function(successcamchat, failurecamchat) {
                                    //alert('regbigimag');
                                    MyHybridBridge.CameraImageValue(successcamchat, failurecamchat, '{"width":321, "height":176,"crop":"Y","caption":"N"}');
                                 },
                                 // plugin call for camera open reg/chat in smaill thumble image done
                                 cameraopenformobilethumb: function(successcamchat, failurecamchat) {
                                 //alert('reg_camesmall');
                                 var getTypeCameraOpen = window.localStorage.getItem("getPicTypeCam");
                                 //alert($scope.getTypeCameraOpen);
                                 if(getTypeCameraOpen=="CHATCAMERA"){
                                    MyHybridBridge.CameraImageValue(successcamchat, failurecamchat, '{"width":152, "height":152,"crop":"N","caption":"Y"}');
                                    window.localStorage.removeItem("getPicTypeCam");
                                 }
                                 else{
                                    MyHybridBridge.CameraImageValue(successcamchat, failurecamchat, '{"width":152, "height":152,"crop":"Y","caption":"N"}');
                                 }

                                 
                                 },
                                 
                                                                  
                                 
                //For Reg Camera Function End
         
                                 
                                 cameraopenforchat: function(successcamchat, failurecamchat) {
                                 MyHybridBridge.CameraImageValue(successcamchat, failurecamchat, '{"width":152, "height":152,"crop":"N","caption":"Y"}');
                                 },
                                 // Call Video function
                                 videorecordingstart: function(successcamchat, failurecamchat) {
                                 CameraVideo.CameraVideoValue(successcamchat, failurecamchat, '{"caption":"Y"}'); //call plugin video recording
                                 },
                                 
                                 // Call Audio function
                                 audiorecordingstart: function(successaudiochat, failureaudiochat) {
                                    audio.AudioRecorder(successaudiochat, failureaudiochat, "Hello"); //call plugin audio recording
                                 },
                                 
                                 // View Video Player function
                                 playerstart: function(successplayer, failureplayer, value) {
                                 playvideo.playvideofunction(successplayer, failureplayer, value); //call plugin videoplayer
                                 },
                                 
                                 
                                 openConfirm: function(opts) {
                                 console.log('openconfirm');
                                 },
                                 close: function() {
                                 
                                 }
                                 };
                                 return publicMethods;
                                 }
                                 ];
                    });
  
  })(window, window.angular);