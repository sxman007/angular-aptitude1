var app = angular.module('app', []);

app.controller('TestController', function($scope) {
	
	$scope.dateChanged =function(){
		
		 alert($scope.startDate);
   alert($scope.enddate);
		
	}
	
  
});

app.directive('datepicker', function() {
  return {
    require: 'ngModel',
    link: function(scope, el, attr, ngModel) {
      $(el).datepicker({
        onSelect: function(dateText) {
          scope.$apply(function() {
            ngModel.$setViewValue(dateText);
          });
        }
      });
    }
  };
});