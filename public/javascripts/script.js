var app = angular.module('test_app',['ui.router']);
app.config(function($urlRouterProvider, $stateProvider){
    //var folderName = "mobile/";
    $urlRouterProvider.otherwise('/home');
    $stateProvider.state('home', {
        url: '/home',
        controller: 'homeController',
        templateUrl: 'partials/home.html'
        //animation: { nextPage:'slideLeft',prvPage:'slideRight'}
    })
    .state('index', {
        url: '/',
        controller: 'homeController',
        templateUrl: 'partials/home.html'
        //animation: { nextPage:'slideLeft',prvPage:'slideRight'}
    })
   
    .state('add', {
        url: '/add',
        controller: 'addController',
        templateUrl: 'partials/add.html'
    });

   


	});
app.controller('homeController',function($scope,$http,$state){
	//alert("TESST");
  //console.log("JAYATISH");
  $http.get("http://localhost:3000/api/todos").then(function(response) {
    	console.log(response);
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear();
      if(dd<10) {
          dd='0'+dd
      } 
      if(mm<10) {
          mm='0'+mm
      } 
      today = mm+'/'+dd+'/'+yyyy;
      console.log(today);
    	$scope.datas=response.data;
      // console.log(new Date());
      //   console.log(response.data);
      //   console.log(response.data[0]);
      //   console.log(response.data[0].deadline);
      //   var date_diff = new Date() - new Date(response.data[0].deadline);
      //   var date_diff = new Date() - new Date(response.data[1].deadline);
      //   console.log(date_diff);
      for(var counter=0;counter<response.data.length;counter++){
        console.log(response.data[counter]);
        var date1 = new Date(today);
        var date2 = new Date(response.data[counter].deadline);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.round(timeDiff / (1000 * 3600 * 24)); 
        var diff_date_value = date2.getTime() - date1.getTime();
        //alert(date2.getTime() - date1.getTime());
        //var date_diff = new Date(response.data[counter].deadline) - new Date();
        if(parseInt(diff_date_value)==0){
          $scope.datas[counter].background_color = '#F9FAA4';
        }else if(parseInt(diff_date_value)<0){
          $scope.datas[counter].background_color = '#FAA4A7';
        }else{
          $scope.datas[counter].background_color = '#eee';
        }
        $scope.datas[counter].time_calc = diffDays;
      }
      console.log($scope.datas);
    });

     $scope.RedirectToURL = function()
	{
	  	
		//alert('hi');
		$state.go('add'); 
	};
   $scope.mark_complete = function(event_id)
	{
	  //alert(event_id);
	   //$window.location.href = 'public/add.html';
	    //alert(event.target.id);
	  var id=event_id;
    $scope.data = {};
    $scope.data.id = id;

	    /*$http.post("http://localhost:3000/api/complete/",{}).then(function(res){
	    	console.log('res samar');
	    	console.log(res);
	    });*/

    $http.post("http://localhost:3000/api/complete",$scope.data)
      .success(function(data){
        $scope.myData = data;
        console.log($scope.myData); // 1
        document.getElementById(id).remove();
        document.getElementById('is_complete_'+id).innerHTML = '<p>YES</p>';
        $state.go('home'); 
    });
	};

    
});

 app.controller('addController',function($scope,$rootScope,$http,$state){
 		$scope.addfunction = function(){
 			$scope.data = {};
 			$scope.data.name = $scope.cusname;
 			$scope.data.deadline = $scope.cusdate;
 			$scope.data.importance = $scope.importance;
      console.log($scope.data);
			$http.post("http://localhost:3000/api/add-todo",$scope.data)
			  .success(function(data){
			    $scope.myData = data;
			    console.log($scope.myData); // 1
			    $state.go('home'); 
			});
 		}

});
app.directive('datepicker', function() {
  return {
    require: 'ngModel',
    link: function(scope, el, attr, ngModel) {
      $(el).datepicker({
        onSelect: function(dateText) {
			
          scope.$apply(function() {
				console.log(dateText);
			 ngModel.$setViewValue(dateText);
          });
        }
      });
    }
  };
});