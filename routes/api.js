var express = require('express');
var api = express.Router();
var apiService = require('../server/services/apiService');
var TodoController=require('../controllers/TodoController');

api.post('/add-todo',function(req,res,next){
    new TodoController().add(req,res,next);
});
api.post('/complete',function(req,res,next){
    new TodoController().mark_complete(req,res,next);
});

api.get('/todos', function(req, res) {
    apiService.getTodos().then(function(todos) {
        return res.json(todos);
    }).catch(function(error) {
        res.statusCode = 500;
        res.send({
            message: "Error getting todos!",
            error: error
        });
    });

});

module.exports = api;
