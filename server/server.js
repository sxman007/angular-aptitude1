var express = require('express');
var path = require('path');
var server = express();
var base = require('../routes/base');
var api = require('../routes/api');
server.use('/stylesheets', express.static(path.resolve(__dirname, '../public/stylesheets')));
server.use('/javascripts', express.static(path.resolve(__dirname, '../public/javascripts')));
server.use('/partials', express.static(path.resolve(__dirname, '../public/partials')));
server.use('/api', api);
server.use('/', base);

process.on('uncaughtException', function(err){
    console.error("Uncaught exception: ", err.stack);
});

process.on('unhandledRejection', function(err){
    console.error("Unhandled rejection", err.stack);
});

module.exports = server;
