var Q = require('q');
var appRoot = require('app-root-path');
module.exports = {
    getTodos: function() {
        var path=CONSTANTS.getDataPath();
        try{
            content = require(path);
            var deferred = Q.defer();
            deferred.resolve(content);
            return deferred.promise;
        }
        catch(e)
        {
            console.log(e);
            return Q.defer().resolve([]);
        }
    },
};
