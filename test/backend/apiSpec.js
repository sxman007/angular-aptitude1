/*jshint browser: false, strict: false, expr: true*/
/*global console, require, module, Report */

var process = require('process');
var apiService = require('../../server/services/apiService');

describe('Node service: apiService', function () {
    it('Should return a list of todos', function () {
        return apiService.getTodos().then(function (todos) {
            expect(todos).to.have.lengthOf(5);
            expect(todos[0].name).to.equal("The first task");
            expect(todos[1].name).to.equal("The second task");
            expect(todos[2].name).to.equal("The third task");
            expect(todos[3].name).to.equal("The fourth task");
            expect(todos[4].name).to.equal("The fifth task");
        });
    });

});
